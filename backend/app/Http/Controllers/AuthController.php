<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;

class AuthController extends Controller
{
    // Registration

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
          'name' => 'required|min:4',
          'email' => 'required|email|unique:users',
          'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
             return response()->json(['message' => $validator->errors()->first()], 400);
         }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $token = $user->createToken('authToken')->accessToken;

        return response()->json(['token' => $token], 200);
    }


    // Login

    public function login(Request $request)
    {

      $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required'
      ]);

      if ($validator->fails()) {
           return response()->json(['message' => $validator->errors()->first()], 400);
       }

       $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

      if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('authToken')->accessToken;
            return response()->json(['token' => $token], 200);

        } else {
            return response()->json(['message' => 'Invalid Credentials'], 401);
        }

    }
}
