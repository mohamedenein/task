<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Image;
use Validator;
use App\Http\Resources\ProductResource;
use Event;
use App\Events\AddProductMail;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();
        return response()->json(ProductResource::collection($products), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|max:255',
          'price' => 'required|numeric',
          'description' => 'required',
          'main_image' => 'required|file|image',
          'other_images.*' => 'required|image|file'
        ]);

        if ($validator->fails()) {
             return response()->json(['message' => $validator->errors()->first()], 400);
         }

        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;

        // upload img
        $imageName = time().'.'.$request->main_image->extension();
        $request->main_image->move(public_path('images'), $imageName);
        $product->main_image = $imageName;
        $product->save();

        //upload other images
        if(isset($request->other_images)) {
          foreach($request->other_images as $file)
            {
                $name = time().rand(1,100).'.'.$file->extension();
                $file->move(public_path('images'), $name);

                $image = new Image;
                $image->product_id = $product->id;
                $image->image = $name;
                $image->save();
            }
        }
        event(new AddProductMail(1));
        return response()->json(new ProductResource($product), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findorfail($id);
        return new ProductResource($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|max:255',
          'price' => 'required|numeric',
          'description' => 'required',
          'main_image' => 'required|image',
          'other_images.*' => 'image'
        ]);

        if ($validator->fails()) {
             return response()->json(['message' => $validator->errors()->first()], 400);
         }

        $product = Product::findorfail($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;

        // upload img
        $imageName = time().'.'.$request->main_image->extension();
        $request->main_image->move(public_path('images'), $imageName);
        $product->main_image = $imageName;
        $product->save();

        //upload other images
        if(isset($request->other_images)) {
          foreach($request->other_images as $file)
            {
                $name = time().rand(1,100).'.'.$file->extension();
                $file->move(public_path('images'), $name);

                $image = new Image;
                $image->product_id = $product->id;
                $image->image = $name;
                $image->save();
            }
        }

        return response()->json(new ProductResource($product), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::findorfail($id)->delete();
        return response()->noContent();
    }
}
