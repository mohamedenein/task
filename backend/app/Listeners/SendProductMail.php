<?php

namespace App\Listeners;

use App\Events\AddProductMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\User;
use Mail;


class SendProductMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddProductMail  $event
     * @return void
     */
    public function handle(AddProductMail $event)
    {
        $user = User::find($event->user_id)->toArray();

        Mail::send('emails.ProductMail', $user, function($message) use ($user) {
          $message->to($user['email']);
          $message->subject('Product Added Successfully');
          $message->from('ecommerce@gmail.com');
        });

    }
}
