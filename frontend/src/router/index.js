import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import AddProduct from "../views/AddProduct.vue";
import EditProduct from "../views/editProduct.vue";



Vue.use(VueRouter);

const routes = [
  {
    path: "/home",
    name: "Home",
    component: Home,
  },
  {
    path: "/product/add",
    name: "AddProduct",
    component: AddProduct,
  },
  {
    path: "/product/edit/:id",
    name: "EditProduct",
    component: EditProduct,
  }

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
